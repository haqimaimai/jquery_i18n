package com.jqueryi18n.xl.controller.index;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String applications(HttpServletRequest request, HttpServletResponse response, Model model) {
		String path = request.getContextPath();
		model.addAttribute("path", path);
		return "index";
	}


}
